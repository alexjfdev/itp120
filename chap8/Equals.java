import java.util.Arrays;
import java.util.ArrayList;

class Equals{
  public static void main(String [] args){
    //var creation
    int intOne = 0;
    int intTwo = 0;
    String stringOne = "test";
    String stringTwo = "test";
    int[] arrayOne = {0,1,2,3};
    int[] arrayTwo = {0,1,2,3};
    ArrayList<Integer> arrayListOne = new ArrayList<>(0);
    ArrayList<Integer> arrayListTwo = new ArrayList<>(0);
    arrayListOne.add(0); arrayListOne.add(1); arrayListOne.add(2);
    arrayListTwo.add(0); arrayListTwo.add(1); arrayListTwo.add(2);
    //int comparison
    if(intOne == intTwo){
      System.out.print(intOne); System.out.print(" == "); System.out.println(intTwo);
    }
    System.out.println(".equals() cannot be used on ints"); System.out.println();
    //string comparison
    if(stringOne == stringTwo){
      System.out.print(stringOne); System.out.print(" == "); System.out.println(stringTwo);
    }
    if(stringOne.equals(stringTwo)){
      System.out.print(stringOne); System.out.print(" .equals "); System.out.println(stringTwo); System.out.println();
    }
    //int array comparison
    if(arrayOne != arrayTwo){
      System.out.print(arrayOne); System.out.print(" != "); System.out.println(arrayTwo);
    }
    if(arrayOne.equals(arrayTwo) == false){
      System.out.print(arrayOne); System.out.print(" does not .equals "); System.out.println(arrayTwo);
    }
    if(Arrays.equals(arrayOne, arrayTwo)){
      System.out.print(Arrays.toString(arrayOne)); System.out.print(" Array.equals() "); System.out.println(Arrays.toString(arrayTwo)); System.out.println();
    }
    //arraylist comparison
    if(arrayListOne != arrayListTwo){
      System.out.print(arrayListOne); System.out.print(" != "); System.out.println(arrayListTwo);
    }
    if(arrayListOne.equals(arrayListTwo)){
      System.out.print(arrayListOne); System.out.print(" .equals "); System.out.println(arrayListTwo);
    }
  }
}
