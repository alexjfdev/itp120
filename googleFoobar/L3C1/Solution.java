import java.util.Arrays;

public class Solution {
    public static int solution(int start, int length) {
      int checksum = 0;
      for(int rowNum=0; rowNum<length; rowNum++){
        for(int val=start; val<length+start-rowNum; val++){
          checksum ^= rowNum*length+val;
        }
      }
    return checksum;
    }
}
