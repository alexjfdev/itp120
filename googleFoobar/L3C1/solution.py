def solution(start, length):
    id_list = []
    for row_num in range(0, length):
        #print(row_num)
        row_list = []
        for val in range(start, length+start-row_num):
            print(val)
            row_list.append(row_num*length+val)
            id_list.append(row_num*length+val)
        print(row_list)

    checksum = id_list[0]
    for val in id_list[1:]:
        checksum ^= val
    #print(id_list)
    print(checksum)
