def solution(elevatorVersionList):
    elevatorVersionsAsListsList = []
    sortedList = []
    for elevatorVersion in elevatorVersionList:
        periodOne = elevatorVersion.find(".")
        periodTwo = elevatorVersion.find(".",periodOne+1)

        if periodOne == -1 & periodTwo == -1:
            majorVersion = int(elevatorVersion)
            minorVersion = -1
            revisionVersion = -1
        elif periodOne != -1 & periodTwo == -1:
            majorVersion = int(elevatorVersion[0:periodOne])
            minorVersion = int(elevatorVersion[periodOne+1:])
            revisionVersion = -1
        else:
            majorVersion = int(elevatorVersion[0:periodOne])
            minorVersion = int(elevatorVersion[periodOne+1:periodTwo])
            revisionVersion = int(elevatorVersion[periodTwo+1:])

        elevatorAsList = [majorVersion,minorVersion,revisionVersion]

        for N in range(0,len(elevatorVersionsAsListsList)+1):
                if N >= len(elevatorVersionsAsListsList):
                    elevatorVersionsAsListsList.append(elevatorAsList)
                    break
                elif elevatorAsList[0] < elevatorVersionsAsListsList[N][0]:
                    elevatorVersionsAsListsList.insert(N,elevatorAsList)
                    break
                elif elevatorAsList[0] == elevatorVersionsAsListsList[N][0]:
                    if elevatorAsList[1] < elevatorVersionsAsListsList[N][1]:
                        elevatorVersionsAsListsList.insert(N,elevatorAsList)
                        break
                    elif elevatorAsList[1] == elevatorVersionsAsListsList[N][1]:
                        if elevatorAsList[2] < elevatorVersionsAsListsList[N][2]:
                            elevatorVersionsAsListsList.insert(N,elevatorAsList)
                            break
                        elif elevatorAsList[2] == elevatorVersionsAsListsList[N][2]:
                            elevatorVersionsAsListsList.insert(N,elevatorAsList)
                            break


    return(str(elevatorVersionsAsListsList)[1:-2].replace(" ","").replace("[","").replace(",",".").replace("].",",").replace(".-1",""))
