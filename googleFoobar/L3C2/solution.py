def solution(input_map):
    solution_list = [input_map]
    max_rows = len(input_map)-1
    max_collumns = len(input_map[0])-1
    solution_list[0].append([False,0,0])
    #Has the bunny broken a wall?, Bunny Collumn, Bunny Row
    steps = 1
    while True:
        steps += 1
        for map_num in range(0,len(solution_list)):
            map = solution_list.pop(0)
            map[map[-1][1]][map[-1][2]]=2
            if (map[-1][2]+1 <= max_collumns) and (map[map[-1][1]][map[-1][2]+1] != 2):
                print("trying right")
                if map[map[-1][1]][map[-1][2]+1] == 1:
                    if map[-1][0] == False:
                        solution_list.append([row[:] for row in map])
                        solution_list[-1][-1][2] += 1
                        if solution_list[-1][-1][1:] == [max_rows,max_collumns]:
                            return steps
                        solution_list[-1][-1][0] = True
                        print("right")
                else:
                    solution_list.append([row[:] for row in map])
                    solution_list[-1][-1][2] += 1
                    if solution_list[-1][-1][1:] == [max_rows,max_collumns]:
                        return steps
                    print("right")

            if (map[-1][1]+1 <= max_rows) and (map[map[-1][1]+1][map[-1][2]] != 2):
                print("trying down")
                if map[map[-1][1]+1][map[-1][2]] == 1:
                    if map[-1][0] == False:
                        solution_list.append([row[:] for row in map])
                        solution_list[-1][-1][1] += 1
                        if solution_list[-1][-1][1:] == [max_rows,max_collumns]:
                            return steps
                        solution_list[-1][-1][0] = True
                        print("down")

                else:
                    solution_list.append([row[:] for row in map])
                    solution_list[-1][-1][1] += 1
                    if solution_list[-1][-1][1:] == [max_rows,max_collumns]:
                        return steps
                    print("down")


            if (map[-1][2]-1 >= 0) and (map[map[-1][1]][map[-1][2]-1] != 2):
                print("trying left")
                if map[map[-1][1]][map[-1][2]-1] == 1:
                    if map[-1][0] == False:
                        solution_list.append([row[:] for row in map])
                        solution_list[-1][-1][2] -= 1
                        if solution_list[-1][-1][1:] == [max_rows,max_collumns]:
                            return steps
                        solution_list[-1][-1][0] = True
                        print("left")

                else:
                    solution_list.append([row[:] for row in map])
                    solution_list[-1][-1][2] -= 1
                    if solution_list[-1][-1][1:] == [max_rows,max_collumns]:
                        return steps
                    print("left")


            if (map[-1][1]-1 >= 0) and (map[map[-1][1]-1][map[-1][2]] != 2):
                print("trying up")
                if map[map[-1][1]-1][map[-1][2]] == 1:
                    if map[-1][0] == False:
                        solution_list.append([row[:] for row in map])
                        solution_list[-1][-1][1] -= 1
                        if solution_list[-1][-1][1:] == [max_rows,max_collumns]:
                            return steps
                        solution_list[-1][-1][0] = True
                        print("up")

                else:
                    solution_list.append([row[:] for row in map])
                    solution_list[-1][-1][1] -= 1
                    if solution_list[-1][-1][1:] == [max_rows,max_collumns]:
                        return steps
                    print("up")


        """for map in solution_list:
            for row in map:
                print(str(row)+",")
            print("")"""

        print("next gen\n\n")
        if len(solution_list) == 0:break
