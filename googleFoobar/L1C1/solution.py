def solution(sequence):
    sequenceLengthFactors = []
    slices = 0

    for i in range(1, len(sequence) + 1):
       if len(sequence) % i == 0:
           sequenceLengthFactors.append(i)
    for factor in sequenceLengthFactors:
        sequenceSplitList = []
        isValid = True
        for i in range(0, len(sequence), factor):
            sequenceSplitList.append(sequence[i:i + factor])
        for elem in sequenceSplitList:
            if elem != sequenceSplitList[0]:
                isValid = False
                break

        if isValid == True:
            slices = len(sequenceSplitList)
            break

    return slices

print ("\n"+str(solution("abcabcabcabc")))
print ("\n"+str(solution("abccbaabccba")))
print ("\n"+str(solution("pseehnpseehnpseehnpseehnpseehnpseehnpseehnpseehnpseehn")))
print ("\n"+str(solution("gnggippfqhpgmotygnggippfqhpgmotygnggippfqhpgmotygnggippfqhpgmotygnggippfqhpgmotygnggippfqhpgmotygnggippfqhpgmotygnggippfqhpgmotygnggippfqhpgmotygnggippfqhpgmotygnggippfqhpgmotygnggippfqhpgmotygnggippfqhpgmotygnggippfqhpgmotygnggippfqhpgmotygnggippfqhpgmoty")))
print ("\n"+str(solution("zqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnnizqtcvxnni")))
print ("\n"+str(solution("")))
print ("\n"+str(solution("a")))
print ("\n"+str(solution("hghghghghghghghghghghghghghghghghghghghghghghghg")))
print ("\n"+str(solution("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")))
