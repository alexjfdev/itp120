def solution(x, y):
    num = 0
    for i in range(1,x+1):
        num += i
    for i in range(x,y+x-1):
        num += i
    return num
