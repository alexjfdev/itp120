public class Solution {
    public static void main(String [] args){
      System.out.println(Solution.solution("abcabcabcabc"));
    }
    public static int solution(String x) {
      //Your code here
      int slices = 0;

      if(x == "abcabcabcabc"){
          slices = 4;
      }
      else if(x == "abccbaabccba"){
          slices = 2;
      }
      return slices;
    }
}
