class Thing {
  int value;

  void displayValue(){
    System.out.println("The value is:"+value);
  }
  void increment(){
    ++value;
    System.out.println("The value has been increased by one to:"+value);
  }
  void decrement(){
    --value;
    System.out.println("The value has been decreased by one to:"+value);
  }
  void change(int amount){
    value += amount;
    System.out.println("The value has been assigned to "+(value-amount)+" plus "+amount+" which equals "+value);
  }
}
