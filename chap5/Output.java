class Output{
  public static void main(String [] args){
    Output o = new Output();
    o.go();
  }
  void go(){
    int y = 7; //integer y = 7
    for(int x = 1; x < 8; x++) { //for loop will repeat 7 times
      y++; //y = y + 1
      if (x > 4){//if x is greater than 4
        System.out.print(++y + " ");//y = y + 1, print y" "
      }
      if (y > 14){//if y is greater than 14
        System.out.println(" x = " + x);//print " x = "x
        break;//stop the for loop
      }
    }
  }
}
/*
This program will print
13 15  x = 6
*/
