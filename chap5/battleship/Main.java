import java.util.ArrayList;

class Main{
  void start(int width, int shipCount, int shipLength){
    Game game = new Game();
    Render render = new Render();
    ArrayList<ArrayList<ArrayList<Integer>>> arrayListBattleships = game.generateMap(width, shipCount-1, shipLength-1);
    //render.renderMap(width, arrayListBattleships, shipLength-1);
    game.start(width, arrayListBattleships);
  }
}
