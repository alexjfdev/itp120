import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

class Game{
  static ArrayList<ArrayList<ArrayList<Integer>>> generateMap(int width, int shipCount, int shipLength){
    Random random = new Random();
    int maxWidth = width-1;
    ArrayList<ArrayList<ArrayList<Integer>>> arrayListBattleships = new ArrayList<>(shipCount);
    for(int shipNumber=0; shipNumber<=shipCount; shipNumber++){
      boolean shipHorizontal = random.nextBoolean();
      int xcoord;
      int ycoord;
      if(shipHorizontal==true){
        xcoord = random.nextInt(maxWidth-4+shipLength);
        ycoord = random.nextInt(maxWidth);
      }
      else{
        xcoord = random.nextInt(maxWidth);
        ycoord = random.nextInt(maxWidth-4+shipLength);
      }
      ArrayList<ArrayList<Integer>> arrayListBattleshipCoords = new ArrayList<>(2);
      for(int coordNumber=0; coordNumber<shipLength; coordNumber++){
        ArrayList<Integer> coordArrayList = new ArrayList<>(1);
        if(shipHorizontal==true){
          coordArrayList.add(xcoord+coordNumber);
          coordArrayList.add(ycoord);
        }
        else{
          coordArrayList.add(xcoord);
          coordArrayList.add(ycoord+coordNumber);
        }
        arrayListBattleshipCoords.add(coordArrayList);
      }
      boolean shouldBreak = false;
      for(int cordNumber=0; cordNumber<shipLength; cordNumber++){
        for(int battleShipNumber=0; battleShipNumber<arrayListBattleships.size(); battleShipNumber++){
          for(int battleShipCordNumber=0; battleShipCordNumber<shipLength; battleShipCordNumber++){
            if(arrayListBattleships.get(battleShipNumber).get(battleShipCordNumber).equals(arrayListBattleshipCoords.get(cordNumber))){
            //if(arrayListBattleships.get(battleShipNumber).get(battleShipCordNumber).get(0) == arrayListBattleshipCoords.get(cordNumber).get(0) && arrayListBattleships.get(battleShipNumber).get(battleShipCordNumber).get(1) == arrayListBattleshipCoords.get(cordNumber).get(1)){
              shouldBreak = true;
              break;
            }
          }
          if(shouldBreak == true){
            break;
          }
        }
        if(shouldBreak == true){
          break;
        }
      }
      if(shouldBreak == true){
        shipNumber-=1;
        continue;
      }
      arrayListBattleships.add(arrayListBattleshipCoords);
    }
    return arrayListBattleships;
  }
  static boolean start(int width, ArrayList<ArrayList<ArrayList<Integer>>> arrayListBattleships){
    Scanner input = new Scanner(System.in);
    Render render = new Render();
    ArrayList<ArrayList<Integer>> hitList = new ArrayList<>(0);
    ArrayList<ArrayList<Integer>> missList = new ArrayList<>(0);
    render.renderMap(width, arrayListBattleships, hitList, missList);
    int guesses = 0;
    while(true){
      String inputString = input.nextLine();
      //render.renderMap(width, arrayListBattleships, hitList, missList);
      int letter = inputString.replaceAll("\\d","").toLowerCase().charAt(0);
      int number = Integer.parseInt(inputString.replaceAll("[^\\d]",""));
      ArrayList<Integer> coordArrayList = new ArrayList<>(1);
      coordArrayList.add(letter-97);
      coordArrayList.add(number);
      if(missList.contains(coordArrayList) || hitList.contains(coordArrayList)){
        System.out.println("You've already guessed that position!!!");
        continue;
      }
      boolean hasHit = false;
      for(int shipNum=0; shipNum<arrayListBattleships.size(); shipNum++){
        for(int cordNum=0; cordNum<arrayListBattleships.get(shipNum).size(); cordNum++){
          if(coordArrayList.equals(arrayListBattleships.get(shipNum).get(cordNum))){
            hasHit = true;
            hitList.add(coordArrayList);
            render.renderMap(width, arrayListBattleships, hitList, missList);
            System.out.println("hit");
            arrayListBattleships.get(shipNum).remove(cordNum);
            if(arrayListBattleships.get(shipNum).size()==0){
              System.out.println("You sunk my battleship!!!");
              arrayListBattleships.remove(shipNum);
            }
            break;
          }
        }
        if(hasHit == true){
          break;
        }
      }
      if(hasHit == false){
        missList.add(coordArrayList);
        render.renderMap(width, arrayListBattleships, hitList, missList);
        System.out.println("miss");
      }

      guesses++;

      if(arrayListBattleships.size() == 0){
        System.out.println("You sunk all my battlships.\nYou win!!!");
        System.out.print("You took:");
        System.out.print(guesses);
        System.out.println(" guesses");
        break;
      }
      //render.renderMap(width, arrayListBattleships, hitList, missList);
    }
    return true;
  }
}
